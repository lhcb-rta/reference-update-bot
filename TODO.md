### We should differentiate messages levels for issues

- WARNING for anything that the shifter might be able to assess alone
- ERROR for anything that is definitely for the maintainer/bot author to check
- We should never throw exceptions so catch RefBotException and give some meaningful feedback

### Where to clone projects and cleanup

- Clone projects in a configurable/temp directory? That would avoid poluting the repo's
  root directory if one's just doing as per the README `poetry run update_references ...`.
- Should clean up after we're done (with the option to keep the clones for debugging).

### References are partially updated in the MR under testing

[Moore!946](https://gitlab.cern.ch/lhcb/Moore/-/merge_requests/946)
affects one existing test `hlt2_reco_calo_selectivematching`
and adds a new one `hlt2_light_reco_calo_efficiency`.

A new reference is committed for `hlt2_light_reco_calo_efficiency.ref` and
`hlt2_reco_calo_selectivematching.ref` is updated such that the tests pass
on all non-v3 platforms.
For v3, `hlt2_light_reco_calo_efficiency.ref.x86_64_v3-opt` is just a copy of
the non-v3 file (so the test fails), and `hlt2_reco_calo_selectivematching.ref.x86_64_v3-opt`
is not modified (so the test fails).

```console
$ git rev-parse origin/master origin/mr/946
ad6887c7e6ad88e7ffdf2630bc93cf9199a9fc75
23e11201d24a8fa3c5d66316150305e90adf15dd
$ git diff --stat origin/master...origin/mr/946
 Hlt/RecoConf/options/hlt2_light_reco_calo_efficiency.py                   |  18 ++++++
 Hlt/RecoConf/options/hlt2_reco_calo_selectivematching.py                  |   3 +-
 Hlt/RecoConf/python/RecoConf/standalone.py                                |  31 ++++++++--
 Hlt/RecoConf/tests/qmtest/hlt2_light_reco_calo_efficiency.qmt             |  32 ++++++++++
 Hlt/RecoConf/tests/refs/hlt2_light_reco_calo_efficiency.ref               | 219 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 Hlt/RecoConf/tests/refs/hlt2_light_reco_calo_efficiency.ref.x86_64_v3-opt | 219 ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 Hlt/RecoConf/tests/refs/hlt2_reco_calo_selectivematching.ref              | 177 +++++++++++++++++++++++++----------------------------
 7 files changed, 598 insertions(+), 101 deletions(-)
```

Running the script gives us
```console
$ git show --stat
commit 08d06924ea06290281ee1bfeffc57a6d4230bafb (HEAD -> ref_bot_Moore946)
Author: Rosen Matev <rosen.matev@cern.ch>
Date:   Tue Aug 10 16:04:13 2021 +0200

    <U+1F916> Update References for: Moore!946

 Hlt/RecoConf/tests/refs/hlt2_light_reco_calo_efficiency.ref.x86_64_v3-opt  | 223 +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 Hlt/RecoConf/tests/refs/hlt2_reco_calo_selectivematching.ref.x86_64_v3-opt | 182 +++++++++++++++++++++++++----------------------------
 2 files changed, 309 insertions(+), 96 deletions(-)
```

which would conflict in `hlt2_light_reco_calo_efficiency.ref.x86_64_v3-opt` if we tried to
merge first !946 and then the reference update commit.

Questions:
- Do we prepare a commit such that it would merge cleanly?
- Are we fine with reference updates being spread across MRs?
- Do we commit (or create a MR) to !946? What if there are multiple MRs in Moore...?
- What if we didn't have the dummy  `hlt2_light_reco_calo_efficiency.ref.x86_64_v3-opt` file? Is a new reference still produced?
