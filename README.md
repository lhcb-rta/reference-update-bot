# Reference Update Bot

Automate the reference updating for tests of the LHCb physics software

## Developing and Using the bot from the shell

Install [Poetry](https://python-poetry.org/docs/) or use the one comming
with LbEnv. Then clone the project and set up the environment:

```console
poetry config --local virtualenvs.in-project true  # optional, helps with VSCode
poetry install  # creates .venv
poetry run pre-commit install
```

Then modify, test and commit as usual.

```console
poetry run pytest
```

Or run the bot from the command line.
```console
poetry run update_references --target=2024-patches -t Rec\!2470 [--debug] [--force] [--create-mrs]
```
Note that the `\` is needed to avoid the shell expansion of `!`
To use `--create-mrs` you need to use a [personal access token](https://gitlab.cern.ch/-/user_settings/personal_access_tokens):
```
export lhcbsoft_gitlab_token=glpat-jlkj436hgghfdsdfDSF
```

---

Structure inspired from [https://github.com/pronovic/apologies].

## Installing CERN certificates

Some websites are signed by the CERN CA (lhcb-couchdb.cern.ch), so we need
to install the CERN certificates (if they are not already in `/etc/pki/tls/certs`)

```console
sudo rpm -i https://linuxsoft.cern.ch/cern/centos/7/cern/x86_64/repoview/CERN-CA-certs.html
sudo update-ca-trust
```
