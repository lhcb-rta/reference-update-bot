import argparse
import logging
import os
import subprocess as sp

log = logging.getLogger(__name__)


# FIXME let's also allow to trigger based on slot, e.g. lhcb-2024-patches-mr/1234
def main() -> None:
    # set `REQUESTS_CA_BUNDLE` (as recent versions of requests use `certifi`
    # rather than the certificates from the system).
    os.environ["REQUESTS_CA_BUNDLE"] = "/etc/pki/tls/certs/ca-bundle.crt"

    from .update_references import update_references
    from .utils import format_warnings, get_gitlab_server, send_gitlab_reply

    parser = argparse.ArgumentParser("Reference updating \N{robot face}")
    parser.add_argument(
        "-t",
        "--trigger",
        type=str,
        required=True,
        help="Specify the MR for which to trigger the bot, e.g. -t Rec\\!1234",
    )
    parser.add_argument(
        "--tmp-dir",
        type=str,
        required=False,
        default="tmp_checkout",
        help="Specify path of the tmp directory to use for the project checkouts",
    )
    parser.add_argument("-d", "--debug", action="store_true", help="Enable debugging output")
    parser.add_argument("--create-mrs", action="store_true", help="Enable creation of MRs and GitLab replies")
    parser.add_argument("--force", action="store_true", help="Ignore some errors and try to continue")
    parser.add_argument(
        "--target",
        type=str,
        required=False,
        default="master",
        help="Target branch for the MR to create",
    )
    parser.add_argument("--build-id", type=int, help="Build id to use for the update (use lastest if not given)")

    args = parser.parse_args()

    logging.basicConfig(
        format="%(levelname)-7s [%(filename)s::%(lineno)s] %(message)s",
        level=(logging.DEBUG if args.debug else logging.INFO),
    )

    trigger_project, trigger_mr_iid = args.trigger.split("!")

    try:
        if not os.path.isdir(args.tmp_dir):
            os.mkdir(args.tmp_dir)

        os.chdir(args.tmp_dir)
        log.debug(f"using temporary directory: {os.getcwd()}")

        projects_to_push, warnings, gitlab_br_name, gitlab_mr_title = update_references(
            trigger_project,
            int(trigger_mr_iid),
            args,
        )

        if args.create_mrs:
            server = get_gitlab_server(authenticate=True)
            new_mrs = []
            for proj in projects_to_push:
                sp.check_call(f"git -C {proj} push --force -o ci.skip origin HEAD", shell=True)

                mr_data = {
                    "source_branch": gitlab_br_name,
                    "target_branch": args.target,
                    "title": gitlab_mr_title,
                    "remove_source_branch": True,
                    "labels": ["reference update"],
                    "description": "",
                }

                mrs = server.projects.get(f"lhcb/{proj}").mergerequests.list(source_branch=gitlab_br_name)
                if mrs:
                    if len(mrs) > 1:
                        raise RuntimeError(f"More than one MR found with source branch {gitlab_br_name}")
                    mr = mrs[0]
                    if mr.state == "closed":
                        mr.state_event = "reopen"
                    for attr, value in mr_data.items():
                        setattr(mr, attr, value)
                    mr.save()
                    new_mrs.append(mr)
                else:
                    new_mrs.append(server.projects.get(f"lhcb/{proj}").mergerequests.create(mr_data))

            reply = (
                f"The [RefBot pipeline]({os.getenv('CI_JOB_URL')}) created the following reference update MRs: "
                + ", ".join([p + "!" + str(mr.iid) for (p, mr) in zip(projects_to_push, new_mrs)])
            )

            if warnings:
                reply += format_warnings(warnings)

            send_gitlab_reply(trigger_project, trigger_mr_iid, reply)
    # usually it isn't good style to catch any exception, but no matter what kind of exception
    # I do want to send a message to GitLab here
    except Exception as e:
        if args.create_mrs:
            log.exception(f"Caught Exception, sending reply to the trigger MR that something failed!\n{e}")
            send_gitlab_reply(
                trigger_project,
                trigger_mr_iid,
                f"The [RefBot pipeline]({os.getenv('CI_JOB_URL')}) failed! :fire: :fire_engine: ",
            )
        else:
            log.exception(f"Caught Exception, but NOT sending GitLab reply!\n{e}")


if __name__ == "__main__":
    main()
