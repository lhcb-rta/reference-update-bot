import logging
import os
import subprocess as sp
from functools import lru_cache, partial
from http.cookiejar import MozillaCookieJar
from tempfile import NamedTemporaryFile
from typing import Any, Callable, Dict, Iterable, List, Set, Tuple, cast

import requests
from gitlab import Gitlab  # type: ignore

log = logging.getLogger(__name__)

JsonDict = Dict[str, Any]


class RefBotException(Exception):
    def __init__(self, msg: str):
        self.msg = msg
        super().__init__(self.msg)

    def __str__(self) -> str:
        return f"\N{POLICE CARS REVOLVING LIGHT}\N{robot face}\N{POLICE CARS REVOLVING LIGHT} {self.msg}"


class LogHistoryHandler(logging.Handler):
    """filters messages with lvl >= WARNING by project and saves them to dictionary"""

    def __init__(self, hist_dict: Dict[str, List[str]], projects: List[str]):
        logging.Handler.__init__(self, logging.WARNING)
        self.hist = hist_dict
        self.projects = sorted(projects, reverse=True)

    def emit(self, record: logging.LogRecord) -> None:
        log_entry = self.format(record)
        for pr in self.projects:
            if record.msg.startswith(pr):
                self.hist[pr].append(log_entry)
                return

        self.hist["General"].append(log_entry)


def handle_processing_error(msg: str, force: bool, logger: logging.Logger) -> None:
    if force:
        logger.error(msg)
    else:
        raise RefBotException(msg)


def rq_method_to_json(rq_method: Callable[..., requests.Response], url: str, **kwargs: Any) -> JsonDict:
    """Make a checked requests call using rq_method and url. If success result is returned as json"""
    resp = rq_method(url, **kwargs)
    try:
        resp.raise_for_status()
        return cast(JsonDict, resp.json())
    except requests.HTTPError:
        log.error(f"Call of requests.{rq_method.__name__} to {url} with {kwargs} failed with code: {resp.status_code}")
        raise


rq_get_to_json = partial(rq_method_to_json, requests.get)
rq_post_to_json = partial(rq_method_to_json, requests.post)


def slot_merges(projects: JsonDict) -> List[Tuple[str, int]]:
    mrs = []
    for proj, meta in projects.items():
        if meta["merges"]:
            mrs += [(proj, x[0]) for x in meta["merges"]]
    return mrs


def requested_slot_merges(mr_slot_meta: JsonDict) -> List[Tuple[str, int]]:
    """return a list of (project_name, MR_id) containing all MRs for requested projects

    Note: the filtering by "requested projects" is done to avoid inclusion of
    MRs that are not connected to the ci-test, e.g. some of the always open MRs
    in Run2Suppor and Gauss.

    """
    mrs = []

    # these are the projects which are actually mentioned in the \ci-test command
    requested_projects = mr_slot_meta["config"]["metadata"]["ci_test"]["requested_projects"]

    for proj in [p for p in mr_slot_meta["config"]["projects"] if p["name"] in requested_projects]:
        mrs += [(proj["name"], mr[0]) for mr in proj["checkout_opts"]["merges"]]

    return mrs


def make_gitlab_mr_branch_name_and_title(mr_slot_meta: JsonDict) -> Tuple[str, str]:
    mrs = requested_slot_merges(mr_slot_meta)
    gitlab_br_name = "ref_bot_" + "_".join([proj + str(iid) for proj, iid in mrs])
    gitlab_title = "Update References for: " + ", ".join([f"{proj}!{iid}" for proj, iid in mrs])

    return gitlab_br_name, gitlab_title


# stolen & adapted from https://gitlab.cern.ch/lhcb-core/lb-jenkins/-/blob/master/lb_jenkins/cern_sso.py
# https://jenkins-lhcb-nightlies.web.cern.ch/job/nightly-builds/job/tests/3106395/api/json?pretty=true
@lru_cache
def get_jenkins_sso_cookie() -> MozillaCookieJar:
    """
    Return a MozillaCookieJar with the authentication cookies for a given URL.

    At the moment this relies on the command cern-get-sso-cookie, using
    Kerberos authentication.
    """

    # FIXME is cern-get-sso-cookie available in gitlab bot?
    # needs kerberos token
    with NamedTemporaryFile() as f:
        ret = sp.check_call(
            [
                "cern-get-sso-cookie",
                "-r",
                "-u",
                "https://jenkins-lhcb-nightlies.web.cern.ch/securityRealm/commenceLogin",
                "-o",
                f.name,
            ]
        )
        if ret != 0:
            raise Exception("FIXME")

        cj = MozillaCookieJar(f.name)
        cj.load()
        return cj


def jenkins_job_is_running(jenkins_url: str) -> bool:
    """
    Given a url to a jenksin build, check if job is still running.

    jenkins_url : url to jenkins job, e.g. : https://jenkins-lhcb-nightlies.web.cern.ch/job/nightly-builds/job/tests/3111946/
    """
    # FIXME our docker image doesn't have cern-get-sso-cookie
    # need to fix this at some point
    # until then we just return True
    return True

    cj = get_jenkins_sso_cookie()
    resp = requests.get(jenkins_url + "/api/json", cookies=cj)
    # FIXME if I don't get a valid respone, return False or True?
    # True is the conservative one as I will abort the entire update and one hast to wait
    # False would mean I treat the job as timed out or aborted
    # not sure what is best TBH. Can this call really fail? I guess we will find out
    # so let's add an extra printout to notice if the call fails
    if not resp:
        log.warning(f"Access to {jenkins_url} failed with code: {resp.status_code}.\n{resp.text}")
    return bool(resp.json()["building"]) if resp else True


GITLAB_URL = "https://gitlab.cern.ch/"


@lru_cache
def get_gitlab_server(url: str = GITLAB_URL, authenticate: bool = False) -> Gitlab:
    if authenticate:
        return Gitlab(url, private_token=os.getenv("lhcbsoft_gitlab_token"))
    else:
        return Gitlab(url)


def send_gitlab_reply(project: str, mr_iid: str, msg: str) -> None:
    try:
        mr = get_gitlab_server(authenticate=True).projects.get(f"lhcb/{project}").mergerequests.get(mr_iid)
        log.debug(f"Sending GitLab reply to {project}!{mr_iid} '{mr.title}':\n{msg}")
        mr.discussions.create({"body": f"{msg}"})
    except Exception as e:
        log.exception(f"Sending GitLab reply failed!\n{e}")


def mr_git_head_changed(project: str, commit1: str, mr_id: int) -> bool:
    proj = get_gitlab_server().projects.get("lhcb/" + project)
    head_sha = proj.mergerequests.get(mr_id).sha
    return bool(proj.repository_compare(commit1, head_sha)["diffs"])  # type: ignore


def mrs_have_changed(projects: JsonDict) -> bool:
    ret = False
    for proj, meta in projects.items():
        for mr in meta["merges"]:
            if mr_git_head_changed(proj, mr[1], mr[0]):
                log.warning(f"{proj}!{mr[0]} changed since ci-test was launched!")
                ret = True

    return ret


def can_be_affected(project: str, mr_projects: Iterable[str], slot_meta: JsonDict) -> bool:
    """Can project's tests be affected by changes in mr_projects?"""
    deps = {p["name"]: p.get("dependencies", []) for p in slot_meta["config"]["projects"]}

    def all_deps(p: str) -> Set[str]:
        return set([p]).union(*(all_deps(d) for d in deps.get(p, [])))

    return bool(all_deps(project).intersection(mr_projects))


def format_warnings(warnings: Dict[str, List[str]]) -> str:
    fold_template = """

<p>
<details>
<summary>Click this to see encountered <b>{proj}</b> warnings </summary>
<pre><code>{proj_warnings}
</code></pre>
</details>
</p>


"""
    # General errors should be printed first
    tmp = warnings.pop("General", None)
    general_errors = [("General", tmp)] if tmp else []
    all_errors = general_errors + sorted(warnings.items())
    return "".join([fold_template.format(proj=p, proj_warnings="\n".join(ws)) for (p, ws) in all_errors])
