import os

import pytest

# Workaround for pytest catching all exceptions when debugging a test
# within VSCode. _PYTEST_RAISE is set in launch.json in the
# "request": "test" configuration. For more information see
# https://stackoverflow.com/a/62563106/1630648
if os.getenv("_PYTEST_RAISE", "0") != "0":

    @pytest.hookimpl(tryfirst=True)
    def pytest_exception_interact(call):  # type: ignore
        raise call.excinfo.value

    @pytest.hookimpl(tryfirst=True)
    def pytest_internalerror(excinfo):  # type: ignore
        raise excinfo.value
