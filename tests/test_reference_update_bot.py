import os

from reference_update_bot import __version__
from reference_update_bot.update_references import parse_test_xml

FIXTURE_DIR = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    "fixtures",
)


def test_version() -> None:
    assert __version__ == "0.1.0"


def test_process_test_xml() -> None:
    xml_path = os.path.join(FIXTURE_DIR, "lhcb-2024-patches-mr/2672/Rec/x86_64_v3-centos7-gcc10-opt/Test.xml")
    with open(xml_path) as f:
        tests = parse_test_xml(f.read(), "Rec/x86_64_v3-centos7-gcc10-opt", False)

    test1 = tests["FunctorCore.example_test"]
    assert test1["Status"] == "passed"
    assert test1["Output Reference File"] == "Phys/FunctorCore/tests/refs/example_test.ref"
    assert test1["New Output Reference File"] == "Phys/FunctorCore/tests/refs/example_test.ref.new"
    assert "Causes" not in test1

    test2 = tests["RichFutureRecSys.pmts-v1.reco-from-dst"]
    assert test2["Status"] == "failed"
    assert test2["Causes"] == "unexpected Wrong 1DHistograms"
    assert "1DHistogramsMismatch" in test2
    assert test2["Output Reference File"] == "Rich/RichFutureRecSys/tests/refs/rich-dst-reco-pmt-v1.ref.x86_64_v3-opt"
    assert (
        test2["New Output Reference File"]
        == "Rich/RichFutureRecSys/tests/refs/rich-dst-reco-pmt-v1.ref.x86_64_v3-opt.new"
    )
